import React from 'react';

import './Header.scss';


const Header = () => (
  <div className="header row align-items-center py-2 text-center">

    <div className="col-6 justify-content-start">
      <div className="row align-items-center justify-content-start">
        <div className="logo-wrapper col-auto">
          <a className="homepage-link" href="/">
            <i className="logo-icon fas fa-ad"></i>
          </a>
        </div>
        <div className="shop-name-wrapper col-auto">
          <p className="text">MyShop</p>
        </div>
      </div>
    </div>

    <div className="col-6">
      <div className="row align-items-center justify-content-end">
        <div className="notification col-auto">
          <i className="fas fa-bell"></i>
        </div>
        <div className="user-wrapper col-auto">
          <p className="text">admin</p>
        </div>
      </div>
    </div>

  </div>
);

export default Header;
