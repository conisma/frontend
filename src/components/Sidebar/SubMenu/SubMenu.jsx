import React from 'react';
import PropTypes from 'prop-types';

import './SubMenu.scss';

const SubMenu = ({ items }) => (
  <ul className="sub-menu-items-wrapper px-3">
    {
      items.map(item => (
        <li key={item.link} className="sub-menu-item mt-5">
          <a href={item.link}>
            {item.title}
          </a>
        </li>
      ))
    }
  </ul>
);

SubMenu.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    link: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
  })),
};

SubMenu.defaultProps = {
  items: [],
};

export default SubMenu;
