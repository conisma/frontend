import React from 'react';

import Header from './components/Header/Header';
import Sidebar from './components/Sidebar/Sidebar';


function App() {
  return (
    <div className="container-fluid">
      <Header/>
      <div className="row">
        <Sidebar/>
      </div>
    </div>
  );
}

export default App;
