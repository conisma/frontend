import React from 'react';

import './Sidebar.scss';
import SubMenu from './SubMenu/SubMenu';


const items = [
  {
    title: 'Retail Dashboard',
    link: '/shit',
  },
  {
    title: 'Sales Reports',
    link: '/some-kind-of-shit',
  },
  {
    title: 'Inventory Reports',
    link: '/another-kind-of-shit',
  },
  {
    title: 'Payment Reports',
    link: '/more-shit',
  },
];


const Sidebar = () => (
  <>
    <div className="sidebar-wrapper d-flex text-center pl-3">
      <div className="menu-items-wrapper d-flex flex-column mr-2">
        <div className="menu-item-wrapper pt-4">
          <div className="menu-item-icon-wrapper icon-home">
            <i className="fas fa-home"></i>
          </div>
          <div className="menu-item-text-wrapper">
            <p>Home</p>
          </div>
        </div>
        <div className="menu-item-wrapper pt-4">
          <div className="menu-item-icon-wrapper icon-sell">
            <i className="fas fa-cash-register"></i>
          </div>
          <div className="menu-item-text-wrapper">
            <p>Sell</p>
          </div>
        </div>
        <div className="menu-item-wrapper pt-4">
          <div className="menu-item-icon-wrapper icon-sales-ledger">
            <i className="fas fa-receipt"></i>
          </div>
          <div className="menu-item-text-wrapper">
            <p>Sales Ledger</p>
          </div>
        </div>
        <div className="menu-item-wrapper pt-4">
          <div className="menu-item-icon-wrapper icon-reporting">
            <i className="fas fa-chart-bar"></i>
          </div>
          <div className="menu-item-text-wrapper">
            <p>Reporting</p>
          </div>
        </div>
        <div className="menu-item-wrapper pt-4">
          <div className="menu-item-icon-wrapper icon-products">
            <i className="fas fa-tags"></i>
          </div>
          <div className="menu-item-text-wrapper">
            <p>Products</p>
          </div>
        </div>
        <div className="menu-item-wrapper pt-4">
          <div className="menu-item-icon-wrapper icon-customers">
            <i className="fas fa-user-friends"></i>
          </div>
          <div className="menu-item-text-wrapper">
            <p>Customers</p>
          </div>
        </div>
        <div className="menu-item-wrapper pt-4">
          <div className="menu-item-icon-wrapper icon-setup">
            <i className="fas fa-cog"></i>
          </div>
          <div className="menu-item-text-wrapper">
            <p>Setup</p>
          </div>
        </div>
      </div>
      <div className="sub-menu-wrapper d-flex flex-column ml-3">
        <SubMenu items={items}/>
      </div>
    </div>
  </>
);

export default Sidebar;
